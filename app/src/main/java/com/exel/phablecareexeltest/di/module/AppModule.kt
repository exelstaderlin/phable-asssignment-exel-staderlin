package com.exel.phablecareexeltest.di.module

import android.content.Context
import com.exel.phablecareexeltest.BaseApplication
import com.exel.phablecareexeltest.dao.UserDao
import com.exel.phablecareexeltest.dao.UserRoomDatabase
import com.exel.phablecareexeltest.di.module.ActivityBuildersModule
import com.exel.phablecareexeltest.di.module.FragmentBuilderModule
import com.exel.phablecareexeltest.di.module.ViewModelModule
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module(
    includes = [ViewModelModule::class,
        ActivityBuildersModule::class,
        FragmentBuilderModule::class]
)
class AppModule {

    @Singleton
    @Provides
    fun provideContext(application: BaseApplication): Context {
        return application.applicationContext
    }

    @Singleton
    @Provides
    fun provideUserDao(application: Context): UserDao {
        return UserRoomDatabase.getDatabase(application).userDao()
    }


}