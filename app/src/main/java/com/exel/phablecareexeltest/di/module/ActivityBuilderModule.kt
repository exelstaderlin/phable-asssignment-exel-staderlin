package com.exel.phablecareexeltest.di.module

import com.exel.phablecareexeltest.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
internal abstract class ActivityBuildersModule {

    @ContributesAndroidInjector
    internal abstract fun bindMainActivity(): MainActivity

}