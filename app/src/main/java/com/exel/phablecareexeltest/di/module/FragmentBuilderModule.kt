package com.exel.phablecareexeltest.di.module

import com.exel.phablecareexeltest.main.FragmentFirst
import com.exel.phablecareexeltest.main.FragmentSecond
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class FragmentBuilderModule{

    @ContributesAndroidInjector
    abstract fun bindFragmentFirst() : FragmentFirst

    @ContributesAndroidInjector
    abstract fun bindFragmentSecond() : FragmentSecond

}