package com.exel.phablecareexeltest.repository

import androidx.lifecycle.LiveData
import com.exel.phablecareexeltest.dao.UserDao
import com.exel.phablecareexeltest.model.UserData
import javax.inject.Inject

class MainRepository @Inject constructor(private val userDao: UserDao) {

    val allUser: LiveData<List<UserData>> = userDao.getAllUser()

    suspend fun insert(user: UserData) {
        userDao.insert(user)
    }

    suspend fun update(user: UserData) {
        userDao.update(user)
    }

    suspend fun delete(user: UserData) {
        userDao.delete(user)
    }
}