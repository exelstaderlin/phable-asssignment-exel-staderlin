package com.exel.phablecareexeltest.main

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.lifecycle.observe
import com.exel.phablecareexeltest.R
import com.exel.phablecareexeltest.dao.UserRoomDatabase
import com.exel.phablecareexeltest.model.UserData
import com.exel.phablecareexeltest.repository.MainRepository
import com.exel.phablecareexeltest.util.ADD_ACTION
import com.exel.phablecareexeltest.util.DELETE_ACTION
import com.exel.phablecareexeltest.util.EDIT_ACTION
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var modelFactory: ViewModelProvider.Factory
    private lateinit var mViewModel: MainViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mViewModel = ViewModelProvider(this, modelFactory)[MainViewModel::class.java]
        initObserver()
        openFragment(FragmentFirst(mViewModel))
    }


    private fun initObserver() {
        mViewModel.eventClickLiveData.observe(this) {
            val action = it["action"] as Int
            val userData = it["user_data"] as UserData
            when (it["action"]) {
                DELETE_ACTION -> mViewModel.delete(userData)
                else -> openFragment(FragmentSecond(action, userData, mViewModel))
            }
        }

    }

    private fun openFragment(fragment: Fragment) {
        val bundle = Bundle()
        fragment.arguments = bundle
        val fragmentManager: FragmentManager = supportFragmentManager
        val fragmentTransaction = fragmentManager.beginTransaction()
        if (fragment is FragmentSecond) {
            fragmentTransaction.setCustomAnimations(
                R.anim.enter_from_right,
                R.anim.exit_to_left,
                R.anim.enter_from_left,
                R.anim.exit_to_right
            ).replace(R.id.frame, fragment).addToBackStack("Second Fragment")
                .commit() //with animation
        } else {
            fragmentTransaction.replace(R.id.frame, fragment).commit() //with no animation
        }
    }

}
