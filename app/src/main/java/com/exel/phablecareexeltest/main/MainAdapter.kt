package com.exel.phablecareexeltest.main

import android.annotation.SuppressLint
import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.exel.phablecareexeltest.databinding.CardUserBinding
import com.exel.phablecareexeltest.model.UserData
import com.exel.phablecareexeltest.util.DELETE_ACTION
import com.exel.phablecareexeltest.util.EDIT_ACTION
import com.exel.phablecareexeltest.util.showBottomDialog


class MainAdapter(var activity: Activity,var mViewModel : MainViewModel, var listData: ArrayList<UserData>) :
    RecyclerView.Adapter<MainAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            CardUserBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(listData[position])
        holder.itemBinding.setOnClickListener {
            showBottomDialog(activity, onEdit = {
                mViewModel.eventClick(EDIT_ACTION, listData[position])
            }, onDelete = {
                mViewModel.eventClick(DELETE_ACTION, listData[position])
            })
        }
    }

    inner class ViewHolder(private val binding: CardUserBinding) : RecyclerView.ViewHolder(binding.root) {
        val itemBinding = binding.root
        fun bind(userData: UserData) {
            binding.apply {
                data = userData
                executePendingBindings()
            }
        }
    }

    override fun getItemCount(): Int {
        return listData.size
    }

}
