package com.exel.phablecareexeltest.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.exel.phablecareexeltest.R
import com.exel.phablecareexeltest.databinding.FragmentSecondBinding
import com.exel.phablecareexeltest.model.UserData
import com.exel.phablecareexeltest.util.ADD_ACTION
import com.exel.phablecareexeltest.util.EDIT_ACTION
import com.exel.phablecareexeltest.util.blankValidator
import com.exel.phablecareexeltest.util.isEmailValid
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_second.*


class FragmentSecond(
    private val mAction: Int,
    private val mUserData: UserData,
    private val mViewModel: MainViewModel
) :
    DaggerFragment() {

    private lateinit var mBinding: FragmentSecondBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = FragmentSecondBinding.inflate(inflater, container, false)
        mBinding.data = mUserData
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        initListener()
    }

    private fun initView() {
        when (mAction) {
            ADD_ACTION -> button.text = getString(R.string.add)
            EDIT_ACTION ->  button.text = getString(R.string.edit)
        }
    }

    private fun initListener() {
        button.setOnClickListener {
            if (
                blankValidator(email_et) &&
                blankValidator(name_et) &&
                isEmailValid(email_et)
            )
            when (mAction) {
                ADD_ACTION -> insertUser()
                EDIT_ACTION -> updateUser()
            }
        }
    }

    private fun insertUser() {
        mUserData.name = name_et.text.toString()
        mUserData.email = email_et.text.toString()
        mViewModel.insert(mUserData)
        activity?.onBackPressed()
    }

    private fun updateUser() {
        mUserData.name = name_et.text.toString()
        mUserData.email = email_et.text.toString()
        mViewModel.update(mUserData)
        activity?.onBackPressed()
    }

}