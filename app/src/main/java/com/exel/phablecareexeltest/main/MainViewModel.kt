package com.exel.phablecareexeltest.main

import androidx.databinding.ObservableBoolean
import androidx.lifecycle.*
import com.exel.phablecareexeltest.model.UserData
import com.exel.phablecareexeltest.repository.MainRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainViewModel @Inject constructor(private var mainRepository: MainRepository) : ViewModel() {

    val allUser: LiveData<List<UserData>> = mainRepository.allUser

    val emptyOrNot =  ObservableBoolean(false)

    val eventClickLiveData = MutableLiveData<HashMap<String, Any>>()

    fun eventClick(action: Int, userData: UserData) {
        val map = HashMap<String, Any>()
        map["action"] = action
        map["user_data"] = userData
        eventClickLiveData.value = map
    }

    fun insert(userData: UserData) = viewModelScope.launch(Dispatchers.IO) {
        mainRepository.insert(userData)
    }

    fun update(userData: UserData) = viewModelScope.launch(Dispatchers.IO) {
        mainRepository.update(userData)
    }

    fun delete(userData: UserData) = viewModelScope.launch(Dispatchers.IO) {
        mainRepository.delete(userData)
    }

}