package com.exel.phablecareexeltest.main

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.observe
import androidx.recyclerview.widget.LinearLayoutManager
import com.exel.phablecareexeltest.databinding.FragmentFirstBinding
import com.exel.phablecareexeltest.model.UserData
import com.exel.phablecareexeltest.util.ADD_ACTION
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_first.*

class FragmentFirst(private var mViewModel: MainViewModel) : DaggerFragment() {

    private lateinit var mAdapter: MainAdapter
    private lateinit var mBinding: FragmentFirstBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = FragmentFirstBinding.inflate(inflater, container, false)
        mBinding.viewModel = mViewModel
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initListener()
        initObserver()
        initMenuAdapter()
    }

    private fun initMenuAdapter() {
        val listUser = ArrayList<UserData>()
        mAdapter = MainAdapter(this.activity as Activity, mViewModel, listUser)
        recycler_view.layoutManager =
            LinearLayoutManager(this.activity, LinearLayoutManager.VERTICAL, false)
        recycler_view.isNestedScrollingEnabled = false
        recycler_view.setHasFixedSize(true)
        recycler_view.adapter = mAdapter
    }

    private fun initObserver() {
        mViewModel.allUser.observe(this) { userData ->
            mViewModel.emptyOrNot.set(userData.isEmpty())
            userData.let {
                mAdapter.listData = userData as ArrayList<UserData>
                mAdapter.notifyDataSetChanged()
            }
        }
    }

    private fun initListener() {
        fab.setOnClickListener {
            mViewModel.eventClick(ADD_ACTION, UserData())
        }
    }

}