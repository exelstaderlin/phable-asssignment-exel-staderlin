package com.exel.phablecareexeltest.util

import android.annotation.SuppressLint
import android.app.Activity
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.exel.phablecareexeltest.R
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.textfield.TextInputEditText
import java.util.regex.Pattern


@SuppressLint("InflateParams")
fun showBottomDialog(
    context: Activity,
    onEdit: () -> Unit,
    onDelete: () -> Unit
) {
    val dialog =  BottomSheetDialog(context)
    val inflater = context.layoutInflater
    val dialogView = inflater.inflate(R.layout.dialog_main, null)

    val editButton = dialogView.findViewById<TextView>(R.id.edit_tv)
    val deleteButton = dialogView.findViewById<TextView>(R.id.delete_tv)

    dialog.setContentView(dialogView)
    dialog.show()
    editButton.setOnClickListener {
        dialog.dismiss()
        onEdit()
    }

    deleteButton.setOnClickListener {
        dialog.dismiss()
        onDelete()
    }
}

fun blankValidator(editText: EditText): Boolean {
    if (editText.text!!.isBlank()) {
        editText.error = "Field must not be blank"
        return false
    }
    return true
}

fun isEmailValid(email: EditText): Boolean {
    val expression =
        "^[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-z0-9])?\\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$"
    val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
    val matcher = pattern.matcher(email.text.toString())
    return if (matcher.matches())
        true
    else {
        email.error = "The email is invalid"
        false
    }
}
