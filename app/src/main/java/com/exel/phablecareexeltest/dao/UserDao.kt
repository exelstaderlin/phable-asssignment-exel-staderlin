package com.exel.phablecareexeltest.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.exel.phablecareexeltest.model.UserData
import dagger.Provides

@Dao
interface UserDao {

    @Query("SELECT * from user_table ORDER BY name ASC")
    fun getAllUser(): LiveData<List<UserData>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(userData: UserData)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(userData: UserData)

    @Delete
    suspend fun delete(userData: UserData)

    @Query("DELETE FROM user_table")
    suspend fun deleteAll()
}